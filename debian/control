Source: r-cran-base64url
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev,
               r-cran-backports (>= 1.1.0)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-base64url
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-base64url.git
Homepage: https://cran.r-project.org/package=base64url

Package: r-cran-base64url
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R fast and URL-safe Base64 encoder and decoder
 In contrast to RFC3548, the 62nd character ("+") is replaced with
 "-", the 63rd character ("/") is replaced with "_". Furthermore, the encoder
 does not fill the string with trailing "=". The resulting encoded strings
 comply to the regular expression pattern "[A-Za-z0-9_-]" and thus are
 safe to use in URLs or for file names.
 The package also comes with a simple base32 encoder/decoder suited for
 case insensitive file systems.
